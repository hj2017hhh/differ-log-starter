package com.logs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.ExampleApplication;
import com.example.domain.Organization;
import com.example.mapper.OrganizationMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExampleApplication.class)
public class DiffOpreateTests {
	@Autowired
	private OrganizationMapper mapper;

	@Test
	public void createOrgTestCase() throws Exception {
		Organization req = new Organization();
		req.setOrgCategory("TEAM");
		req.setOrgName("DEV公司");
		req.setParentOrgCode("1");
		req.setMerchantCode("897374847478998");
		req.setOrgExt("添加数据");
		req.setOrgStatus((byte)1);
		req.setMobile("18402850503");
		req.setIndependentAccountability((byte)1);
		mapper.insertUseGeneratedKeys(req);
	}

	@Test
	public void modifyOrgTestCase() throws Exception {
		Organization record = new Organization();
		record.setOrgName("99999999公司");
		record.setMobile("7777");
		record.setOrgLevel((byte)2);
		record.setId(27L);
		mapper.updateByPrimaryKeySelective(record);
	}

	@Test
	public void disabelOrg() throws Exception {
		Organization record = new Organization();
		record.setOrgStatus((byte)0);
		record.setId(30L);
		mapper.updateByPrimaryKeySelective(record);
	}

	@Test
	public void enableOrg() throws Exception {
		Organization record = new Organization();
		record.setOrgStatus((byte)1);
		record.setId(11L);
		mapper.updateByPrimaryKeySelective(record);
	}
	
	@Test
	public void deleteOrg() throws Exception {
		mapper.deleteByPrimaryKey(30L);
	}

}