package com.logs.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.ExampleApplication;
import com.example.domain.OrgAccount;
import com.example.mapper.OrgAccountMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExampleApplication.class)
public class DiffOpreate2Tests {
	@Autowired
	private OrgAccountMapper mapper;

	@Test
	public void createTestCase() throws Exception {
		OrgAccount req = new OrgAccount();
		req.setAccountName("xx分公司对公账号");
		req.setAccountNo("8388485856");
		req.setBankName("中国农业银行");
		req.setBankCardMobile("18402850503");
		mapper.insertUseGeneratedKeys(req);
	}

	@Test
	public void modifyTestCase() throws Exception {
		OrgAccount req = new OrgAccount();
		req.setId(1L);
		req.setAccountName("yy分公司对公账户");
		req.setAccountNo("938584858585858");
		req.setBankName("中国工商银行");
		req.setBankCardMobile("18076575691");
		mapper.updateByPrimaryKeySelective(req);
	}


}