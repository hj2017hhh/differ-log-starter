package com.example.domain;

import com.differ.log.anno.FieldDesc;
import com.differ.log.anno.ModelDesc;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** * t_org
 * @author Administrator 
 *2019-12-22
 */
@Table(name = "t_org")
@ModelDesc("t_org")
@Data
public class Organization implements Serializable {
    /**
     * 代理主键,自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @FieldDesc("代理主键,自增")
    private Long id;

    /**
     * 机构类型[ORG、TEAM]
     */
    @Column(name = "org_category")
    @FieldDesc("机构类型[ORG、TEAM]")
    private String orgCategory;

    /**
     * 机构等级[1 2 3]
     */
    @Column(name = "org_level")
    @FieldDesc("机构等级[1 2 3]")
    private Byte orgLevel;

    /**
     * 机构编码
     */
    @Column(name = "org_code")
    @FieldDesc("机构编码")
    private String orgCode;

    /**
     * 上级机构编码
     */
    @Column(name = "parent_org_code")
    @FieldDesc("上级机构编码")
    private String parentOrgCode;

    /**
     * 机构名称
     */
    @Column(name = "org_name")
    @FieldDesc("机构名称")
    private String orgName;

    /**
     * 商户编码
     */
    @Column(name = "merchant_code")
    @FieldDesc("商户编码")
    private String merchantCode;

    /**
     * 联系电话
     */
    @Column(name = "mobile")
    @FieldDesc("联系电话")
    private String mobile;

    /**
     * 机构扩展信息
     */
    @Column(name = "org_ext")
    @FieldDesc("机构扩展信息")
    private String orgExt;

    /**
     * 机构状态[0 1]
     */
    @Column(name = "org_status")
    @FieldDesc("机构状态[0 1]")
    private Byte orgStatus;

    /**
     * 是否独立核算[0 1]
     */
    @Column(name = "independent_accountability")
    @FieldDesc("是否独立核算[0 1]")
    private Byte independentAccountability;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    @FieldDesc("创建时间")
    private Date createdTime;

    /**
     * 最后更新时间
     */
    @Column(name = "last_updated_time")
    @FieldDesc("最后更新时间")
    private Date lastUpdatedTime;

    /**
     * 删除标识[0:未删除 1:逻辑删除]
     */
    @Column(name = "deleted")
    @FieldDesc("删除标识[0:未删除 1:逻辑删除]")
    private Byte deleted;

    /**
     * 乐观锁
     */
    @Column(name = "optimistic_lock")
    @FieldDesc("乐观锁")
    private Integer optimisticLock;

    /**
     * t_org
     */
    private static final long serialVersionUID = 1L;
}