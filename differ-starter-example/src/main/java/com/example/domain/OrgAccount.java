package com.example.domain;

import com.differ.log.anno.FieldDesc;
import com.differ.log.anno.ModelDesc;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/** * t_org_account
 * @author Administrator 
 *2019-12-22
 */
@Table(name = "t_org_account")
@ModelDesc("t_org_account")
@Data
public class OrgAccount implements Serializable {
    /**
     * 代理主键,自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @FieldDesc("代理主键,自增")
    private Long id;

    /**
     * 账户类型[ORG、PERSION]
     */
    @Column(name = "category")
    @FieldDesc("账户类型[ORG、PERSION]")
    private String category;

    /**
     * 机构编码
     */
    @Column(name = "org_code")
    @FieldDesc("机构编码")
    private String orgCode;

    /**
     * 账户名
     */
    @Column(name = "account_name")
    @FieldDesc("账户名")
    private String accountName;

    /**
     * 开户行
     */
    @Column(name = "bank_name")
    @FieldDesc("开户行")
    private String bankName;

    /**
     * 账号
     */
    @Column(name = "account_no")
    @FieldDesc("账号")
    private String accountNo;

    /**
     * 银行预留手机号
     */
    @Column(name = "bank_card_mobile")
    @FieldDesc("银行预留手机号")
    private String bankCardMobile;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    @FieldDesc("创建时间")
    private Date createdTime;

    /**
     * 最后更新时间
     */
    @Column(name = "last_updated_time")
    @FieldDesc("最后更新时间")
    private Date lastUpdatedTime;

    /**
     * 删除标识[0:未删除 1:逻辑删除]
     */
    @Column(name = "deleted")
    @FieldDesc("删除标识[0:未删除 1:逻辑删除]")
    private Byte deleted;

    /**
     * 乐观锁
     */
    @Column(name = "optimistic_lock")
    @FieldDesc("乐观锁")
    private Integer optimisticLock;

    /**
     * t_org_account
     */
    private static final long serialVersionUID = 1L;
}