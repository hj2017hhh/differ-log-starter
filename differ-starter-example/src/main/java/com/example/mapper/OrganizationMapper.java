package com.example.mapper;

import com.differ.log.tk.BaseMapper;
import com.example.domain.Organization;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrganizationMapper extends BaseMapper<Organization> {
}