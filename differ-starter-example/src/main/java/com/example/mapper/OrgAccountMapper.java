package com.example.mapper;

import com.differ.log.tk.BaseMapper;
import com.example.domain.OrgAccount;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrgAccountMapper extends BaseMapper<OrgAccount> {
}