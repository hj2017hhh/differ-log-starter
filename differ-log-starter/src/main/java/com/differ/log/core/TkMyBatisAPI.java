package com.differ.log.core;

public interface TkMyBatisAPI {

	public static final String insertUseGeneratedKeys="insertUseGeneratedKeys";
	
	public static final String updateByPrimaryKeySelective="updateByPrimaryKeySelective";
	
	public static final String deleteByPrimaryKey="deleteByPrimaryKey";
}
