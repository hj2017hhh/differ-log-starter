package com.differ.log.core.dto;

import java.util.ArrayList;
import java.util.List;

import com.differ.log.domain.Log;
import com.differ.log.domain.LogContent;

import lombok.Data;

@Data
public class LogBean {

	private Log log=new Log();
	private List<LogContent> logContents=new ArrayList<LogContent>();
}
