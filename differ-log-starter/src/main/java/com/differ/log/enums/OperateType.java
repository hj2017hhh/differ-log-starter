package com.differ.log.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public enum OperateType {
    ADD((byte)1,"新增"),
    UPDATE((byte)2,"修改"),
    DELEET((byte)3,"刪除");
	private byte value;
	private String desc;
	public byte getValue() {
		return value;
	}
	public String getDesc() {
		return desc;
	}
	public static OperateType match(Byte type) {
		OperateType[] values = OperateType.values();
		for (int i = 0; i < values.length; i++) {
			if(type==values[i].getValue()) {
				return values[i];
			}
		}
		return null;
	}
	
	
}
