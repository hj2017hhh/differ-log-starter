package com.differ.log.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.differ.log.api.LogController;
import com.differ.log.core.DiffUtils;
import com.differ.log.core.LogAspect;
import com.differ.log.dao.DifferDao;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import({
	LogController.class,
	DiffUtils.class,
	LogAspect.class,
	DifferDao.class
})
public @interface EnableDifferLog {

}
