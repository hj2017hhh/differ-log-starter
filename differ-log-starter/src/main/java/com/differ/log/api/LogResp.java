package com.differ.log.api;

import java.util.List;

import com.differ.log.domain.Log;
import com.differ.log.domain.LogContent;
import com.differ.log.enums.OperateType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class LogResp extends Log{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<LogContent> logContents;
	
	public String getTypeCn() {
		if(super.getType()!=null) {
			OperateType operateType = OperateType.match(super.getType());
			if(operateType!=null) {
				return operateType.getDesc();
			}
		}
		return null;
	}
}
