package com.differ.log.api;

import java.io.Serializable;

/**
 * 返回给客户端的统一交互视图
 */
public class RestResultVo implements Serializable {

    private static final long serialVersionUID = 827560194166970585L;

    private int code;

    private String msg;

    private Integer count;

    private Object data;

    public static RestResultVo fail() {
        return new RestResultVo(500, "操作失败");
    }

    public static RestResultVo fail(String message) {
        return new RestResultVo(500, message);
    }

    public static RestResultVo fail(int code, String message) {
        return new RestResultVo(code, message);
    }

    public static RestResultVo succeed() {
        return new RestResultVo(200, "OK");
    }

    public static RestResultVo succeed(Object body) {
        return new RestResultVo(200, "OK", body);
    }

    public RestResultVo() {
    }

    public RestResultVo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public RestResultVo(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
