package com.differ.log.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.differ.log.dao.DifferDao;
import com.differ.log.domain.Log;
import com.differ.log.domain.LogContent;

@RequestMapping("/log")
@RestController
public class LogController {
	@Autowired
	private DifferDao differDao;

	@GetMapping("/logList")
	public RestResultVo logList() {
		List<Log> logs = differDao.selectAllLog();
		LogAPIResp logAPIResp = null;
		if (!CollectionUtils.isEmpty(logs)) {
			List<LogResp> logResps = new ArrayList<LogResp>();
			for (Log log : logs) {
				LogResp resp = buildLogResp(log);
				logResps.add(resp);
			}
			logAPIResp = LogAPIResp.builder().logs(logResps).build();
		}
		return RestResultVo.succeed(logAPIResp);
	}

	private LogResp buildLogResp(Log log) {
		if (log != null) {
			LogResp resp = new LogResp();
			BeanUtils.copyProperties(log, resp);
			List<LogContent> logContents = differDao.selectLogContentByLogId(log.getId());
			resp.setLogContents(logContents);
			return resp;
		}
		return null;
	}

	@GetMapping("/logDetail/{id}")
	public RestResultVo logDetail(@PathVariable("id") Long id) {
		Log log = differDao.selectLogRespByPrimaryKey(id);
		return RestResultVo.succeed(buildLogResp(log));

	}
}
