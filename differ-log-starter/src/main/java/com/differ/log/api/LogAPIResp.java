package com.differ.log.api;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LogAPIResp {

	private List<LogResp> logs;
	
}
