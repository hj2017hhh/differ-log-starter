package com.differ.log.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/** * tb_log
 * @author Administrator 
 *2019-12-21
 */
@Data
public class Log implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 管理员id
     */
    private Long adminId;

    /**
     * 操作类型：1新增2修改3删除
     */
    private Byte type;

    /**
     * 主键
     */
    private Long tableId;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date creatDate;

    /**
     * tb_log
     */
    private static final long serialVersionUID = 1L;
}