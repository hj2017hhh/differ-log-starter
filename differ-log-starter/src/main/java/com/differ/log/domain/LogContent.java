package com.differ.log.domain;

import java.io.Serializable;

import lombok.Data;

/**
 * * tb_log_content
 * 
 * @author Administrator 2019-12-21
 */
@Data
public class LogContent implements Serializable {
	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 外键，关联tb_log表
	 */
	private Long logId;

	/**
	 * 修改字段
	 */
	private String tableColumn;
	/**
	 * 旧值
	 */
	private String oldValue;
	/**
	 * 新值
	 */
	private String newValue;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * tb_log_content
	 */
	private static final long serialVersionUID = 1L;
}