CREATE TABLE `tb_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `admin_id` bigint(20) DEFAULT NULL COMMENT '管理员id',
  `type` tinyint(4) DEFAULT '1' COMMENT '操作类型：1新增2修改3删除',
  `table_id` bigint(20) DEFAULT NULL COMMENT '主键',
  `table_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '表名',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `creat_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `tb_log_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `log_id` bigint(20) DEFAULT NULL COMMENT '外键，关联tb_log表',
  `table_column` longtext COLLATE utf8_unicode_ci COMMENT '修改字段',
  `old_value` longtext COLLATE utf8_unicode_ci COMMENT '旧值',
  `new_value` longtext COLLATE utf8_unicode_ci COMMENT '新的值',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;