# differ-log-starter

#### 介绍
基于tkmybatis的数据库字段变更日志记录器starter

#### 软件架构

- springboot-jdbc
- mybatis-generator
- tkmybatis
- aop
- java-object-diff


#### 安装教程
用法详细见[示例工程](https://gitee.com/hj2017hhh/differ-log-starter/tree/master/differ-starter-example)

#### 注意
    当前starter目前只支持mapper的insertUseGeneratedKeys，updateByPrimaryKeySelective，deleteByPrimaryKey方法记录日志，待完善！！
#### 1.在pom.xml中加入仓库以及依赖
      <repositories><!-- 代码库 -->
        <repository>
          <id>git-maven</id>
          <url>https://gitee.com/hj2017hhh/qxsy-depository/tree/master/resps</url>
        </repository>
     </repositories>
     <dependency>
        <groupId>com.differ.log</groupId>
        <artifactId>differ-log-starter</artifactId>
        <version>2.0.0</version>			
     </dependency>
#### 2.执行[戳这里](https://gitee.com/hj2017hhh/differ-log-starter/blob/master/differ-log-starter/db/init.sql)脚本到你的数据库
#### 3.springboot启动类加上@EnableDifferLog
#### 4.配置generatorConfig.xml
 
        <!-- 指定生成的java文件的编码,没有直接生成到项目时中文可能会乱码 -->
		<property name="javaFileEncoding" value="UTF-8" />
		<plugin type="com.differ.log.plugs.LombokPlugin"></plugin>
	    <!-- 生成的‘实体类’将implements Serializable -->
		<plugin type="org.mybatis.generator.plugins.SerializablePlugin"></plugin>
        <!-- 生成的‘Mapper类’将extend BaseMapper-->
		<plugin type="com.differ.log.plugs.MyMapperPlugin">
			<property name="mappers"
				value="com.differ.log.tk.BaseMapper" />
		</plugin>
		<!--生成的‘实体类’将加上starter所需的注解-->
		<commentGenerator
			type="com.differ.log.plugs.MyCommentGenerator">
		</commentGenerator>
#### 5.运行generatorConfig.xml生成Mapper和domain，xml文件